# SPDX-FileCopyrightText: 2021 Anuket contributors
#
# SPDX-License-Identifier: Apache-2.0

""" for docs
"""

# pylint: disable=import-error
# flake8: noqa
from docs_conf.conf import *
copyright = '2021, Anuket. Licensed under CC BY 4.0'
